<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'markuptest' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'O:4fvVhi2l&7De,GN4NLI9Pf;xizusN#3XatP0U;brIhj5UtbIr d4{i&%vw-eu?' );
define( 'SECURE_AUTH_KEY',  '~`)Y-NtI448=y%J5uftZPW`/T|DxV dbW=ul^i5LV;Expwk)eQUeD}ZRuSh}#<c.' );
define( 'LOGGED_IN_KEY',    '#m(xmen5Sb;UxjIhIKyQ{7:G-`h)$[/TQaxgZDzgz.u^~Za?n9fy`|sV^V_DzK.E' );
define( 'NONCE_KEY',        'p2Quch`0z,&}Nfq}gw?m};-i5)@ ^#=}}EtjA7IBYm^L5=`Ce$Ez3s_*JIcUyCbm' );
define( 'AUTH_SALT',        'vIH7D=6z9) ?)IoaWE7X%#q)8qiNeL/lzX?[wyBtOmxv-+#i%6-M+2L$aqn[u64r' );
define( 'SECURE_AUTH_SALT', 'G^#70hT*PVh+/g$ZdZ,o(~RfH$6kyRX{o6 [QP//r2OrN,;N*o&iT{()LVIq]X+i' );
define( 'LOGGED_IN_SALT',   '+/p%;JhUR&y6JhKzki.<G1`X|DJxBbnOG3o5YD}qO!u>/iS,O?G1<D(h3[(!`}wU' );
define( 'NONCE_SALT',       '[[c&TH;=IQa;lD5D.on[3CWCyu8LhfJOTV_t84WvL%:yz`C9oLR1OwQf0|l|P?kM' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
